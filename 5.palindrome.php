<?php

/**
 * Ecrire le code permettant de déterminer si un mot est un palindrome
 * https://fr.wikipedia.org/wiki/Palindrome
 *
 * 1/ Demander à l'utilisateur de saisir un mot
 * 2/ Déterminer si ce mot est un palindrome et afficher :
 * - c'est un palindrome
 * ou
 * - ce n'est pas un palindrome
 */

echo "Veuillez saisir un mot : ";
$mot = trim(fgets(STDIN));

$rmot = strrev ($mot); // renverse
$tabrmot = str_split($rmot); // mise en tableau

$fillmot = "";

foreach($tabrmot as $value){
    $fillmot .= $value;
}

if($fillmot == $mot){
    echo "C'est un Palindrome \n";
} else {
    echo "Ce n'est pas un Palindrome \n";
}
