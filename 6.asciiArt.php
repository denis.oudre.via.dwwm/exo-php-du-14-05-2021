<?php

/**
 * Ecrire le code permettant de dessiner les figures ci-dessous :
 *
 * #####
 * #####
 * #####
 * #####
 * #####
 *
 * #####
 * #   #
 * #   #
 * #   #
 * #####
 *
 * #   #
 *  # #
 *   #
 *  # #
 * #   #
 *
 * #
 * ##
 * # #
 * #  #
 * #####
 *
 *   #
 *  # #
 * #   #
 *  # #
 *   #
 *
 * # # #
 *  # #
 * # # #
 *  # #
 * # # #
 *
 * 1/ Demander à l'utilisateur la hauteur de la figure
 * 2/ Demander à l'utilisateur la largeur de la figure
 * 3/ Demander à l'utilisateur le caractère utilisé pour dessiner la figure
 * 4/ Demander à l'utilisateur quelle figure dessiner (valeurs possibles de 1 à 6)
 * 5/ Dessiner la figure
 *
 */

echo "Veuillez saisir la hauteur : ";
$haut = trim(fgets(STDIN));

echo "Veuillez saisir la largeur : ";
$larg = trim(fgets(STDIN));

echo "Veuillez saisir le caractère à utiliser : ";
$car = trim(fgets(STDIN));

etiq1:
echo "Veuillez saisir le type de dessin (valeur possible de 1 à 6) : ";
$dess = trim(fgets(STDIN));

switch ($dess) {
    case 1:
        // ecriture de la 1ere figure
        for ($cc = 0; $cc < $haut; $cc++) {
            for ($cl = 0; $cl < $larg; $cl++) {
                echo $car;
            }
            echo "\n";
        }
        echo "\n";
        break;
    case 2:
        // ecriture de la 2eme figure
        for ($cc = 0; $cc < $haut; $cc++) {
            if ($cc == 0 || $cc == $haut - 1) {
                for ($cl = 0; $cl < $larg; $cl++) {
                    echo $car;
                }
                echo "\n";
            }
            while ($cc < $haut - 2) {
                echo $car;
                for ($i = 0; $i < $cl - 2; $i++) {
                    echo " ";
                }
                echo $car;
                echo "\n";
                $cc++;
            }
        }
        echo "\n";
        break;
    case 3:
        // ecriture de la 3eme figure
        $nbspext = 0;
        $nbspint = $larg;
        $sens = 1;
        $nblign = 0;
        while ($nblign < $haut) {
            if ($sens === 1){
                $nbspint--;
                $nbspint--;
                if ($nbspint < 0){
                    $sens = 0;
                }
            }
            else if ($sens === 0){
                $nbspint++;
                $nbspint++;
            }
            if ($nbspext !== 0){
                for ($c = 0 ; $c < $nbspext ; $c++){
                    echo ' ';
                }
            }
            echo $car;
            for ($b = 0; $b < $nbspint; $b++) {
                echo " ";
            }
            if ($nbspint > 0){
                echo $car;
            }
            if ($nbspext !== 0){
                for ($c = 0 ; $c < $nbspext ; $c++){
                    echo ' ';
                }
            }
            echo "\n";
            if ($sens > 0){
                $nbspext++;
            }
            else {
                $nbspext--;
            }
            $nblign++;
        }
        break;
    case 4:
        $nblign = 0;
        $nbspint =0;
        while ($nblign < $haut-1) {
            if ($nblign === 0) {
                echo $car;
                echo "\n";
                $nblign++;
            }
            if ($nblign == 1) {
                echo $car . $car;
                $nblign++;
                $nbspint++;
                echo "\n";
            }
            if ($nblign > 1) {
                echo $car;
                for ($a = 0; $a < $nbspint; $a++) {
                    echo " ";
                }
                echo $car;
                $nblign++;
                $nbspint++;
                echo "\n";
            }
        }
        for ($b = 0 ; $b < $larg ; $b++){
            echo $car;
        }
        echo "\n";
        break;
    case 5:
        $nbspext = $larg--;
        $nbspint = 0;
        $nblign = 0;
        $div = $haut/2;
        while ($nblign < $haut) {
            for ($a = 0; $a < $nbspext; $a++) {
                echo " ";
            }
            echo $car;
            if ($nbspint > 0) {
                for ($b = 0 ; $b < $nbspint ; $b++) {
                    echo " ";
                }
            }
            if ($nbspint > 0) {
                echo $car;
            }
            for ($a = 0; $a < $nbspext; $a++) {
                echo " ";
            }
            echo "\n";
            $nblign++;
            if ($nblign < $div){
                $nbspext--;
                $nbspint++;
                if ($nbspint > 1){
                    $nbspint++;
                }
            }
            else {
                $nbspext++;
                $nbspint--;
                if ($nbspint > 1){
                    $nbspint--;
                }
            }
        }
        echo "\n";
        break;
    case 6:
        $nbspext = 1;
        $nbcar = $larg-2;
        $nblign = 0;
        $sens = 0;
        while ($nblign < $haut){
            for ($b = 0 ; $b < $nbspext ; $b++){
                echo " ";
            }
            for ($a = 0 ; $a < $nbcar ; $a++){
                echo $car;
                echo " ";
            }
            for ($b = 0 ; $b < $nbspext ; $b++){
                echo " ";
            }
            if ($sens === 0){
                $nbspext++;
                $nbcar--;
                $sens++;
            }
            else if ($sens === 1){
                $nbspext--;
                $nbcar++;
                $sens--;
            }
            $nblign++;
            echo "\n";
        }
        break;
    default:
        echo "Vous devez rentrer une valeur entre 1 et 6";
        goto etiq1;
        break;

}