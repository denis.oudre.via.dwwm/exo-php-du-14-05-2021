<?php

/**
 * Ecrire un algorithme qui calcul la vitesse moyenne d'un déplacement
 * 1/ Demander à l'utilisateur de saisir la distance parcourue (en km)
 * 2/ Demander à l'utilisateur de saisir le temps pour effectuer le parcours (en mn)
 * 3/ Afficher la vitesse moyenne du déplacement en km/h
 */

//$input = fgets(STDIN);

echo "Quelle est la distance parcourue ? (en km) ";
$dist = trim(fgets(STDIN));

echo "Temps pour effectuer le parcours ? (en mn) ";
$tps = trim(fgets(STDIN));

echo "\n";

if (is_numeric($dist) && is_numeric($tps) ) {
    $vitmoy = ($dist/$tps)*60;
    echo "La vitesse moyenne du déplacement est de ".$vitmoy. "km/h \n";
} else{
    echo "Impossible !"."\n";
}

