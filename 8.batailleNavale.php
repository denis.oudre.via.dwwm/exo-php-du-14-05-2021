<?php

/**
 * Créer le code d'un jeu de bataille navale
 * https://fr.wikipedia.org/wiki/Bataille_navale_(jeu)
 *
 * 8.1 avec un plateau de 16 cases (4 colonnes, 4 lignes) et un seul bateau de 1 case
 * 1/ le programme détermine aléatoirement quelle case du plateau contient le bateau (déterminer la colonne et la ligne)
 *
 * 2/ Afficher le plateau selon le format ci-dessous (chaque case est représentée par un caractère):
 *      ????
 *      ????
 *      ????
 *      ????
 *
 * 3/ Demander à l'utilisateur de deviner où peut se situer le bateau
 *  3.1/ Demander la colonne
 *  3.2/ Demander la ligne
 *
 * 4/ Vérifier si la case choisie par l'utilisateur est bien celle qui contient le bateau
 *  4.1/ Si c'est la bonne case, afficher "Touché coulé \n Bravo vous avez gagné!"
 *  4.2/ Si ce n'est pas la bonne case, afficher "Plouf! A l'eau." et réexécuter les étapes 2 et 3
 *      Les cases déjà mentionnées par l'utilisateur doivent s'afficher avec le caractère ~
 *      Ex si l'utilisateur devine la case située dans la première colonne et la troisième ligne, l'affichage du
 *      plateau sera :
 *      ????
 *      ????
 *      ~???
 *      ????
 *
 * 5/ Afficher combien de tentatives ont été nécessaires pour gagner
 *
 * 6/ Reproposer une partie à l'utilisateur
 *  6.1/ S'il accepte, veiller à ce que le bateau soit repositionné et toutes les cases du plateau soient à
 *      nouveau masquées
 *
 */

debut:

$tableau = [];
$nbcoups = 0;

// remplissage tableau
$b = 0;
for ($a = 0 ; $a < 4 ; $a++) {
        $tableau["lig" . $a] = ["col0" => "?","col1" => "?","col2" => "?","col3" => "?"];
}

// bateau placé alléatoirement
$col = random_int(0,3);
$lig = random_int(0,3);

etiq1:

// affichage du tableau (sans montrer le bateau)
echo "Nombre de coup(s) joué(s) : ".$nbcoups."\n";
echo "\e[1;33m  0 1 2 3\e[0m\n";
$r= 0;
foreach ($tableau as $ligne => $colonne){
    echo "\e[1;33m".$r." \e[0m";
    foreach($colonne as $values => $value){
        if ($value === "x"){
            echo "\e[1;31;44m" . $value . " \e[0m";
        }
        else {
            echo "\e[1;30;44m" . $value . " \e[0m";
        }
    }
    $r++;
    echo "\n";
}

// On demande colonne et ligne


etiq2:
    echo "Rentrer la colonne :  ";
    $colgive = trim(fgets(STDIN));
if (!is_numeric($colgive) || $colgive < 0 || $colgive > 3){
    goto etiq2;
}
etiq3:
    echo "Rentrer la ligne :  ";
    $liggive = trim(fgets(STDIN));
if (!is_numeric($liggive) || $liggive < 0 || $liggive > 3){
    goto etiq3;
}
// on regarde si à cet endroit est le bateau
$nbcoups++;

if ($colgive == $col && $liggive == $lig){
    echo "\n Touché coulé \n Bravo vous avez gagné! (En ".$nbcoups." coups) \n";
    // affichage du tableau avec la position du bateau coulé
    $leclig = $tableau["lig".$liggive];
    $tablec = array();
    for ($i = 0 ; $i < 4 ; $i++){
        $verif = $leclig["col".$i];
        $tablec [] = $verif;
    }
    $tablec [$colgive] = "B";
    $tableau["lig".$liggive] = ["col0" => $tablec[0],"col1" => $tablec[1],"col2" => $tablec[2],"col3" => $tablec[3]];
    echo "\e[1;33m  0 1 2 3\e[0m\n";
    $r= 0;
    foreach ($tableau as $ligne => $colonne){
        echo "\e[1;33m".$r." \e[0m";
        foreach($colonne as $values => $value){
            if ($value === "x"){
                echo "\e[1;31;44m" . $value . " \e[0m";
            }
            elseif ($value === "B"){
                echo "\e[1;33;41m" . $value . " \e[0m";
            }
            else {
                echo "\e[1;30;44m" . $value . " \e[0m";
            }
        }
        $r++;
        echo "\n";
    }
    // On demande si le joueur veut rejouer ou pas
    echo "Voulez vous rejouer ? (O/n) :";
    $rejoue = trim(fgets(STDIN));
    $rejoue = strtoupper($rejoue);
    if ($rejoue !== "N"){
        goto debut;
    }
    else {goto fin;}
}
else {
    echo "Plouf! A l'eau.";
    echo "\n";
// On récupére la ligne du tableau correspondante avant de la modifier
    $leclig = $tableau["lig".$liggive];
    $tablec = array();
    for ($i = 0 ; $i < 4 ; $i++){
        $verif = $leclig["col".$i];
        $tablec [] = $verif;
    }
// on met la case cochée
    $tablec [$colgive] = "x";
// on met à jour la ligne correspondante dans le tableau
    $tableau["lig".$liggive] = ["col0" => $tablec[0],"col1" => $tablec[1],"col2" => $tablec[2],"col3" => $tablec[3]];
    echo "\n";
    goto etiq1;
}
fin:
echo "\n";

