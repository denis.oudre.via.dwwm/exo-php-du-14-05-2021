<?php

/**
 * Créer le code permettant de simuler le fonctionnement d'une machine à café.
 *
 * Le café coûte 60 centimes
 * La machine accepte les pièces de 2€, 1€, 0.5€, 0.2€, 0.1€ et 0.05€
 *
 * 1/ Demander à l'utiliser de saisir la valeur de sa pièce.
 *  - ne pas accepter les pièces non autorisées
 *  - tant que le montant n'est pas supérieur ou égal au prix du café,
 *  demander à l'utilisateur de saisir une autre pièce (le montant doit se cumuler)
 * 2/ Servir le café (bien chaud !)
 * 3/ Rendre la monnaie
 *  - on suppose que le stock de pièce de la machine n'est pas limité
 *  - rendre le moins de pièces possibles
 */

$coffeePrice = 60;
$acceptedCoins = [
    "2e" => 200,
    "1e" => 100,
    "50cts" => 50,
    "20cts" => 20,
    "10cts" => 10,
    "5cts" => 5,
];

$insertedCoins = [];

$dessin = [
"        )  (",
"      (   ) )",
"       ) ( (",
"      _______)_",
"   .-'---------|",
"  ( C|/\/\/\/\/|",
"   '-./\/\/\/\/|",
"     '_________'",
"      '-------'",
"---------------------",
];
echo "          )  (
        (   ) )
         ) ( (
       _______)_
    .-'---------|  
   ( C|/\/\/\/\/|
    '-./\/\/\/\/|
      '_________'
       '-------'
---------------------\n";

$totalpiece = 0;

do {
    do {
        echo "Rentrer la valeur de la pièce ";
        $piece = trim(fgets(STDIN));
        $coinok = array_key_exists($piece, $acceptedCoins);
    } while (!$coinok);
    $insertedCoins[] = $acceptedCoins[$piece];
    $totalpiece = array_sum($insertedCoins);
}while ($totalpiece < 60);
echo "Prépation de votre café";
echo "\n";
foreach ($dessin as $draw){
    sleep (1);
    echo $draw."\n";
}
echo "\n";
echo "Votre café est prêt ! Attention c'est chaud !!";
echo "\n";
$monnaie = $totalpiece - 60;
etiq1:
if ($monnaie > 0){
    $cpt = 0;
    $rangepiece = [];
    $fin = 0;
    foreach ($acceptedCoins as $key => $value){
        $rangepiece [] = $value;
        $diff =$value - $monnaie;
        if ($fin == 0){
            if ($diff <= 0){
                $trouve = $rangepiece[$cpt];
                $donne = array_search($trouve,$acceptedCoins);
                echo "Laisse tomber une piece de ".$donne."\n";
                $monnaie = $monnaie - $trouve;
                $fin = 1;
            }
        }
        $cpt++;
    }
}
if ($monnaie > 0){
    $fin = 0;
    goto etiq1;
}
else {
    echo "Bon café ! \n";
}


