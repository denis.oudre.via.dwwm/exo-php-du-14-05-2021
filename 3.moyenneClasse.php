<?php

/** 3.1
 * Ecrire un algorithme permettant de calculer la moyenne des notes d'une classe.
 * 1/ demande à l'utilisateur de saisir l'ensemble des notes de la classe
 * Tant que l'utilisateur entre un nombre entre 0 et 20, on répète l'instruction.
 * 2/ afficher la moyenne des notes
 */

/** 3.2
 * 3/ Afficher le taux de notes supérieures ou égales à 10
 */

$cpt = 0;
$cptaux = 0;
$totnot = 0;

etiq1:
echo "Quel est la note ? : ";
$note = trim(fgets(STDIN));

if (!is_numeric($note)) {
    goto etiq2;
}
else {
    if (0<= $note && $note <= 20){
        $cpt++;
        $totnot += $note;
        if ($note >= 10){
            $cptaux++;
        }
        goto etiq1;
    }
    else {
        goto etiq2;
    }
}
etiq2:
$moy = $totnot/$cpt;
$taux = (100/$cpt)*$cptaux;
echo "La moyenne de la classe est de ".$moy.". \n";
echo "Taux de note > 10 : ".$taux."%. \n";