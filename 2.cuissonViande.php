<?php

/**
 * Ecrire un algorithme pour afficher le temps de cuisson d'une viande
 * en fonction du type de viande, du poids et de la cuisson souhaitée
 *
 * Règles à appliquer :
 * Pour cuire 500g de boeuf, il faut :
 *  - bleu : 10 minutes
 *  - à point : 17 minutes
 *  - bien cuit : 25 minutes
 * Pour cuire 400g de porc, il faut :
 *  - bleu : 15 minutes
 *  - à point : 25 minutes
 *  - bien cuit : 40 minutes
 * le temps de cuisson est proportionnel au poids
 * (ex: pour cuire 750g de boeuf bleu, il faut 15 minutes)
 *
 * 1/ Demander à l'utilisateur de saisir le type de viande (boeuf, porc)
 * 2/ Demander à l'utilisateur de saisir le poids de la viande à cuire en grammes
 * 3/ Demander à l'utilisateur de saisir la cuisson souhaitée (bleu, à point, bien cuit)
 * 4/ Afficher le temps de cuisson
 *
 * Attention: Lors de la saisie, tant que les valeurs ne sont pas valides,
 * poser de nouveau la question à l'utilisateur.
 * (ex: si le nom de la viande n'est pas "boeuf" ou "porc", demander à nouveau le type de viande)
 */

etiq1:
echo "Quel est le type de viande (boeuf, porc) : ";
$type = trim(fgets(STDIN));

if ($type !== 'boeuf' && $type !== "porc"){
    echo "mauvaise saisie, choisissez boeuf ou porc ! \n";
    goto etiq1;
}
etiq2:
echo "Donnez le poids de la viande à cuire (en grammes): ";
$poids = trim(fgets(STDIN));

if (!is_numeric($poids)){
    echo "Mauvaise saisie, cela ne doit être que des chiffres ! \n";
    goto etiq2;
}
etiq3:
echo "Quelle cuisson désirez vous ? (bleu, à point, bien cuit) : ";
$cuir = trim(fgets(STDIN));

if ($cuir !== 'bleu' && $cuir !== "à point" && $cuir !== "bien cuit"){
    echo "mauvaise saisie, redonnez votre cuisson désirée ! \n";
    goto etiq3;
}
if ($type === "boeuf"){
    switch ($cuir) {
        case "bleu":
            $tpscuimin = (500 / 10);
            $tpscui = $poids / $tpscuimin;
            echo "Le temps de cuisson pour du " . $type . " pesant " . $poids . " grammes est de " . $tpscui . " minutes \n";
            break;
        case "à point":
            $tpscuimin = (500 / 17);
            $tpscui = $poids / $tpscuimin;
            echo "Le temps de cuisson pour du " . $type . " pesant " . $poids . " grammes est de " . $tpscui . " minutes \n";
            break;
        case "bien cuit":
            $tpscuimin = (500 / 25);
            $tpscui = $poids / $tpscuimin;
            echo "Le temps de cuisson pour du " . $type . " pesant " . $poids . " grammes est de " . $tpscui . " minutes \n";
            break;
    }
}
if ($type === "porc"){
    switch ($cuir) {
        case "bleu":
            $tpscuimin = (400 / 15);
            $tpscui = $poids / $tpscuimin;
            echo "Le temps de cuisson pour du " . $type . " pesant " . $poids . " grammes est de " . $tpscui . " minutes \n";
            break;
        case "à point":
            $tpscuimin = (400 / 25);
            $tpscui = $poids / $tpscuimin;
            echo "Le temps de cuisson pour du " . $type . " pesant " . $poids . " grammes est de " . $tpscui . " minutes \n";
            break;
        case "bien cuit":
            $tpscuimin = (400 / 40);
            $tpscui = $poids / $tpscuimin;
            echo "Le temps de cuisson pour du " . $type . " pesant " . $poids . " grammes est de " . $tpscui . " minutes \n";
            break;
    }
}