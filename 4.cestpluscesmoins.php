<?php
/** 4.1
 * Ecrire le code d'un jeu permettant de deviner un nombre.
 * 1/ Demander à l'utilisateur de saisir le nombre minimum possible
 * 2/ Demander à l'utilisateur de saisir le nombre maximum possible
 * 3/ Le programme choisit un nombre aleatoire compris entre ces deux nombres
 * mais ne l'affiche pas à l'utilisateur
 * 4/ le programme demande à l'utilisateur de saisir un nombre et lui répond :
 * - C'est plus
 * - C'est moins
 * - Bravo la réponse était ...
 */
/** 4.2
 * 5/ Demander à l'utilisateur si il souhaite rejouer (sans quitter le programme)
 * si l'utilisateur rejoue, on conserve les valeurs min et max indiquées en 1/ et 2/
 */

/** 4.3
 * 6/ L'utilisateur dispose de 10 tentatives pour deviner le nombre
 * lorsqu'il dépasse 10 tentative le jeu se termine et indique :
 * - Vous avez perdu
 * puis demande à l'utilisateur si il souhaite rejouer
 */
$cpt = 0;

etiq1:
echo "Quel est le nombre minimum possible ? ";
$min = trim(fgets(STDIN));

echo "Quel est le nombre maximum possible ? ";
$max = trim(fgets(STDIN));

if (is_numeric($min) && is_numeric($max) ) {
    if ($min < $max){
        $nbalea = random_int($min, $max);
        goto etiq2;
    }
    else{
        echo "Votre chiffre min est supérieur à max, recommencer \n";
        goto etiq1;
    }
} else{
    echo "Une erreur dans votre saisie vous n'avez pas rentré un chiffre, recommencer"."\n";
    goto etiq1;
}
etiq2:
$cpt++;
if ($cpt === 11){
    echo "Vous avez épuisé vos tentatives (10), vous avez perdu ! Il fallait trouver ".$nbalea."\n";
    echo "Rejouer ? Y/n ";
    $rep = trim(fgets(STDIN));
    goto etiq3;
}

echo "Trouver le nombre compris entre ".$min." et ".$max." : (tentative ".$cpt."/10) ";
$ess = trim(fgets(STDIN));

if (!is_numeric($ess)){
    echo "Dommage un essai perdu à cause d'une mauvaise frappe !";
    goto etiq2;
}
else {
    switch ($ess){
        case ($ess>$nbalea):
            echo "C'est moins que ".$ess."\n";
            goto etiq2;
        case ($ess<$nbalea):
            echo "C'est plus que ".$ess."\n";
            goto etiq2;
        default:
            echo "Bravo la réponse était ".$nbalea." (trouvé en ".$cpt."tentatives) \n";
            echo "Rejouer ? Y/n ";
            $rep = trim(fgets(STDIN));
            break;
    }
}
etiq3:
if ($rep !== "n"){
    $nbalea = random_int($min, $max);
    $cpt = 0;
    goto etiq2;
}
else{
    echo "Merci d'avoir joué \n";
}

